const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');
const Schema = mongoose.Schema;

const schema = Schema({
  _project_id: Schema.Types.ObjectId,
  _narrative: String,
  _state: String,
  _approved: Boolean,
  _priority: String,
  _size: Number,
  _how: String,
  _i_want: String,
  _so_that: String,
  _criteria: String,
  _given: String,
  _when: String,
  _then: String,
  _estimated_time: Number,
  _timeStamp: Date
});

class Story{

  constructor(project_id, narrative, state, approved, priority, size, how, i_want, so_that, criteria, given, when, then, estimated_time, timeStamp){
    this._project_id = project_id;
    this._narrative = narrative;
    this._state = state;
    this._approved = approved;
    this._priority = priority;
    this._size = size;
    this._how = how;
    this._i_want = i_want;
    this._so_that = so_that;
    this._criteria = criteria;
    this._given = given;
    this._when = when;
    this._then = then;
    this._estimated_time = then;
    this._timeStamp = timeStamp;
  }

  get project_id(){
    return this._project_id;
  }

  set project_id(v){
    this._project_id = v;
  }

  get narrative(){
    return this._narrative;
  }

  set narrative(v){
    this._narrative = v;
  }

  get state(){
    return this._state;
  }

  set state(v){
    this._state = v;
  }

  get approved(){
    return this._approved;
  }

  set approved(v){
    this._approved = v;
  }

  get priority(){
    return this._priority;
  }

  set priority(v){
    this._priority = v;
  }

  get size(){
    return this._size;
  }

  set size(v){
    this._size = v;
  }

  get how(){
    return this._how;
  }

  set how(v){
    this._how = v;
  }

  get i_want(){
    return this._i_want;
  }

  set i_want(v){
    this._i_want = v;
  }

  get so_that(){
    return this._so_that;
  }

  set so_that(v){
    this._so_that = v;
  }

  get criteria(){
    return this._criteria;
  }

  set criteria(v){
    this._criteria = v;
  }

  get given(){
    return this._given;
  }

  set given(v){
    this._given = v;
  }

  get when(){
    return this._when;
  }

  set when(v){
    this._when = v;
  }

  get then(){
    return this._then;
  }

  set then(v){
    this._then = v;
  }

  get estimated_time(){
    return this._estimated_time;
  }

  set estimated_time(v){
    this._estimated_time = v;
  }

  get timeStamp(){
    return this._timeStamp;
  }

  set timeStamp(v){
    this._timeStamp = v;
  }
}

schema.plugin(mongoosePaginate);
schema.loadClass(Story);
module.exports = mongoose.model('Story', schema);
