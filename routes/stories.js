var express = require('express');
var router = express.Router();
const storiesController = require('../controllers/storiesController');
const authCheck = require('../config/authCheck');

router.get('/one/:id', storiesController.one);
router.get('/:id', authCheck, storiesController.index);
router.get('/show/:id', authCheck, storiesController.show);
router.post('/new', authCheck, storiesController.create);
router.put('/edit/:id', authCheck, storiesController.update);
router.delete('/delete', authCheck, storiesController.destroy);

module.exports = router;
