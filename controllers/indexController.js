const express = require('express');

function index(req, res, next) {
  res.render('index', {
    baseUrl: req.baseUrl,
    routePath: req.route.path,
    currentUser: req.user,
    title: 'Scrum Project',
    description: 'DRAW course final project'
  });
}

function login(req, res, next) {
  if(!req.user){
    res.render('login', {
      baseUrl: req.baseUrl,
      routePath: req.route.path,
      currentUser: req.user,
      title: 'Scrum Project',
    });
  } else {
    res.redirect('/');
  }
}

function signup(req, res, next) {
  if(!req.user){
    res.render('signup', {
      baseUrl: req.baseUrl,
      routePath: req.route.path,
      currentUser: req.user,
      title: 'Scrum Project',
    });
  } else {
    res.redirect('/');
  }
}

module.exports = {
  index,
  login,
  signup
}
