const express = require('express');
const Story = require('../models/story');
const {validationResult} = require('express-validator/check');

function index(req, res, next) {
  Story.find({
    _project_id: req.params.id
  }).sort({
    _timeStamp: 'desc'
  }).then( obj =>{
    return res.status(200).json({
      errors:[],
      data : obj
    });
  }).catch( err =>{
    res.status(500).json({
      errors:[{message:'Something went wrong!'}],
      data : []
    });
  });
}

function one(req, res, next) {
  Story.findOne({
    _id: req.params.id
  }).then( obj =>{
    res.status(200).json({
      errors:[],
      data : obj
    });
  }).catch( err =>{
    res.status(500).json({
      errors:[{message:'Something went wrong!'}],
      data : []
    });
  });
}

function create(req, res, next) {
  const errors = validationResult(req);

  if(!errors.isEmpty()){
    return res.status(422).json({
      errors:errors.array()
    });
  }

  let timestamp = new Date();
  let story = new Story({
    _project_id: req.body.project_id,
    _narrative: req.body.narrative,
    _state: 'CREATED',
    _approved: false,
    _priority: req.body.priority,
    _size: req.body.size,
    _how: req.body.how,
    _i_want: req.body.i_want,
    _so_that: req.body.so_that,
    _criteria: req.body.criteria,
    _given: req.body.given,
    _when: req.body.when,
    _then: req.body.then,
    _estimated_time: req.body.estimated_time,
    _timeStamp: timestamp
  });
  story.save()
  .then( obj =>{
    return res.status(200).json({
      errors: [],
      data: obj
    });
  }).catch( err =>{
    return res.status(500).json({
      errors:[{message: 'something is wrong!'}],
      data:[err]
    });
  });
}

function show(req, res, next) {
  Story.findById(req.params.id).then( obj =>{
    res.status(200).json({
      errors:[],
      story : obj
    });
  }).catch( err =>{
    res.status(500).json({
      errors:[{message:'Something went wrong!!'}],
      data : []
    });
  })
}

function update(req, res, next) {
  Story.findById(req.params.id)
  .then( obj =>{
    obj.project_id = req.body.project_id? req.body.project_id : obj.project_id;
    obj.narrative = req.body.narrative? req.body.narrative : obj.narrative;
    obj.state = req.body.state? req.body.state : obj.state;
    obj.approved = req.body.approved;
    obj.priority = req.body.priority? req.body.priority : obj.priority;
    obj.size = req.body.size? req.body.size : obj.size;
    obj.how = req.body.how? req.body.how : obj.how;
    obj.i_want = req.body.i_want? req.body.i_want : obj.i_want;
    obj.so_that = req.body.so_that? req.body.so_that : obj.so_that;
    obj.criteria = req.body.criteria? req.body.criteria : obj.criteria;
    obj.given = req.body.given? req.body.given : obj.given;
    obj.when = req.body.when? req.body.when : obj.when;
    obj.then = req.body.then? req.body.then : obj.then;
    obj.estimated_time = req.body.estimated_time? req.body.estimated_time : obj.estimated_time;
    obj.save()
    .then( obj => {
      res.status(200).json({
        errors:[],
        data : obj
      });
    }).catch( err =>{
      res.status(500).json({
        errors:[{message:'Something went wrong!!'}],
        data : []
      });
    })
  }).catch( err =>{
    res.status(500).json({
      errors:[{message:'Something went wrong!!'}],
      data : []
    });
  });
}

function destroy(req, res, next) {
  Story.deleteOne({_id: req.body.id})
  .then( obj =>{
    res.status(200).json({
      errors:[],
      data:obj
    })
  }).catch( err =>{
    res.status(500).json({
      errors:[{message:'Something went wrong!'}],
      data : []
    });
  });
}

module.exports = {
  index,
  one,
  create,
  show,
  update,
  destroy
}
