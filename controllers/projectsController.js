const express = require('express');
const Project = require('../models/project');
const User = require('../models/user');
const Story = require('../models/story');
const {validationResult} = require('express-validator/check');

function index(req, res, next) {
  let page = req.params.page ? req.params.page : 1;
  let users;

  const options = {
    page: page,
    limit: 10,
    select: '_name _description _projectManager _productOwner _team _isOpen',
    sort: {_timeStamp: 'desc'}
  };

  //Users with id and name for reference on create and edit projects
  User.find({},{_id: 1, _email: 1}).sort({ _email: 1})
  .then((obj) => {
    users = obj;
  }).catch(() => {
    res.status(500).json({
      errors: [{ message: 'Something went wrong!!'}],
      data: [],
    });
  });

  Project.paginate({
    $or: [
      {_projectManager: req.user.id},
      {_productOwner: req.user.id},
      {_team: req.user.id}
    ]
  }, options, (err, result) => {})
  .then((objs) => {
    Project.countDocuments({
      $or: [
        {_projectManager: req.user.id},
        {_productOwner: req.user.id},
        {_team: req.user.id}
    ]}).then((data) => {
      res.render('projects/index', {
        baseUrl: req.baseUrl,
        routePath: req.route.path,
        currentUser: req.user,
        title: 'Projects',
        description: 'Show all projects',
        users: users,
        projects: objs,
        projectsCount: data
      });
    }).catch(() => {
      res.status(500).json({
        errors: [{ message: 'Something went wrong!!'}],
        data: [],
      });
    });
  })
  .catch(() => {
    res.status(500).json({
      errors: [{ message: 'Something went wrong!'}],
      data: [],
    });
  });
}

function create(req, res, next) {
  const errors = validationResult(req);

  if(!errors.isEmpty()){
    return res.status(422).json({
      errors:errors.array()
    });
  }

  let timestamp = new Date();
  let project = new Project({
    _name: req.body.name,
    _applicationDate: req.body.applicationDate,
    _startDate: req.body.startDate,
    _description: req.body.description,
    _projectManager: req.body.projectManager,
    _productOwner: req.body.productOwner,
    _team: req.body.team,
    _timeStamp: timestamp,
    _isOpen: true,
    _retrospective: {well:'', wrong:'', improve:''}
  });

  project.save()
  .then( obj =>{
    return res.status(200).json({
      errors: [],
      data: obj
    });
  }).catch( err =>{
    return res.status(500).json({
      errors:[{message: 'something is wrong'}],
      data:[err]
    });
  });
}

function show(req, res, next) {
  let users;

  //Users with id and name for reference on create and edit projects
  User.find({},{_id: 1, _email: 1}).sort({ _email: 1})
  .then((obj) => {
    users = obj;
  }).catch(() => {
    res.status(500).json({
      errors: [{ message: 'Something went wrong!!'}],
      data: [],
    });
  });

  Project.findById(req.params.id).then(project =>{
    User.findOne({_id: project.projectManager}).then((projectManager) => {
      User.findOne({_id: project.productOwner}).then((productOwner) => {
        User.find({ _id: { $in: project.team }}).then((team) => {
          Story.find({ _project_id: project.id }).then((stories) =>{
            let estimated_time = 0;

            stories.forEach((story) => {
              estimated_time += story.estimated_time
            });

            console.log(estimated_time);

            res.render('projects/show', {
              baseUrl: req.baseUrl,
              currentUser: req.user,
              routePath: req.route.path,
              title: project.name,
              description: 'obj.description',
              project: project,
              projectManager: projectManager,
              productOwner: productOwner,
              team: team,
              users: users,
              estimated_time: estimated_time
            });
          }).catch( err =>{
            res.status(500).json({
              errors:[{message:'Something went wrong!'}],
              data : []
            });
          });
        }).catch( err =>{
          res.status(500).json({
            errors:[{message:'Something went wrong!!'}],
            data : []
          });
        });
      }).catch( err =>{
        res.status(500).json({
          errors:[{message:'Something went wrong!!'}],
          data : []
        });
      });
    }).catch( err =>{
      res.status(500).json({
        errors:[{message:'Something went wrong!!'}],
        data : []
      });
    });
  }).catch( err =>{
    res.status(500).json({
      errors:[{message:'Something went wrong!!'}],
      data : []
    });
  })
}

function update(req, res, next) {
  Project.findById(req.params.id)
  .then( obj =>{
    obj.name = req.body.name? req.body.name : obj.name;
    obj.applicationDate = req.body.applicationDate? req.body.applicationDate : obj.applicationDate;
    obj.startDate = req.body.startDate? req.body.startDate : obj.startDate;
    obj.projectManager = req.body.projectManager? req.body.projectManager : obj.projectManager;
    obj.productOwner = req.body.productOwner? req.body.productOwner : obj.productOwner;
    obj.team = req.body.team? req.body.team : obj.team;
    obj.retrospective = req.body.retrospective? req.body.retrospective : obj.retrospective;
    obj.save()
    .then( obj => {
      res.status(200).json({
        errors:[],
        data : obj
      });
    }).catch( err =>{
      res.status(500).json({
        errors:[{message:'Something went wrong!!'}],
        data : []
      });
    })
  }).catch( err =>{
    res.status(500).json({
      errors:[{message:'Something went wrong!!'}],
      data : []
    });
  });
}

function changeState(req, res, next){
  Project.findById(req.params.id)
  .then( obj => {
    if (obj.isOpen) {
      obj.isOpen = false;
    } else {
      obj.isOpen = true;
    }
    obj.save()
    .then( obj => {
      res.status(200).json({
        errors:[],
        data : obj
      });
    }).catch( err =>{
      res.status(500).json({
        errors:[{message:'Something went wrong!!'}],
        data : []
      });
    })
  }).catch( err =>{
    res.status(500).json({
      errors:[{message:'Something went wrong!!'}],
      data : []
    });
  })
}

function destroy(req, res, next) {
  if(req.user.id == req.body.manager){
    Project.remove({_id: req.body.id})
    .then( obj =>{
      res.status(200).json({
        errors:[],
        data:obj
      })
    }).catch( err =>{
      res.status(500).json({
        errors:[{message:'Something went wrong!!'}],
        data : []
      });
    });
  } else {
    res.status(500).json({
      errors:[{message:"You can't destroy a project that you don't own"}],
      data : []
    });
  }

}

module.exports = {
  index,
  create,
  show,
  update,
  destroy,
  changeState
}
