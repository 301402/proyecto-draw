$(document).ready(() => {
  // Serialize form to JSON
  $.fn.serializeFormJSON = function () {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
      if (o[this.name]) {
        if (!o[this.name].push) {
          o[this.name] = [o[this.name]];
        }
        o[this.name].push(this.value || '');
      } else {
        o[this.name] = this.value || '';
      }
    });
    return o;
  };

  // Tooltip initializer
  $(function () {
    $('[data-toggle="tooltip"]').tooltip()
  })

  // Turn card for stories
  $('.turn-card').on('click', function(){
    $card = $('.card');

    $card.find('div.front, div.back').removeClass('overflow-hidden');
    setTimeout(() => {
      $card.find('div.front, div.back').addClass('overflow-hidden');
    }, 500);

    if($card.hasClass('rotate')){
      $card.find('div.front').toggleClass('display-none');
      $card.removeClass('rotate');
    } else {
      $card.addClass('rotate');
      $card.find('div.back').toggleClass('overflow-hidden');
      setTimeout(() => {
        $card.find('div.front').toggleClass('display-none');
      }, 500);
    }
  })

  //Create Users
  $(".new-user-ajax").on('click', () => {
    $form = $('#new-user-modal').find('form');
    url = $form.attr('action');
    abilities = $form.find("#user-abilities").multipleSelect("getSelects");
    jsonForm = $form.serializeFormJSON();
    delete jsonForm["abilities"];
    jsonForm.abilities = abilities;
    $.ajax({
      method: 'POST',
      url: url,
      dataType: 'json',
      contentType: 'application/json',
      data: JSON.stringify(jsonForm),
      success: function( response ) {
        location.reload();
      }
    })
  })

  //Edit Users
  $(".edit-user-ajax").on('click', () => {
    $form = $('#edit-user-modal').find('form');
    url = $form.attr('action');
    abilities = $form.find("#user-abilities").multipleSelect("getSelects");
    jsonForm = $form.serializeFormJSON();
    delete jsonForm["abilities"];
    jsonForm.abilities = abilities;
    $.ajax({
      method: 'PUT',
      url: url,
      dataType: 'json',
      contentType: 'application/json',
      data: JSON.stringify(jsonForm),
      success: function( response ) {
        location.reload();
      }
    })
  })

  //Delete Users
  $(".delete-user-ajax").on('click', () => {
    id = $('.delete-user-ajax').attr('data-id');
    $.ajax({
      method: 'DELETE',
      url: '/users/delete',
      contentType: "application/x-www-form-urlencoded; charset=UTF-8",
      data: { id: id },
      success: function( response ) {
        location.reload();
      }
    })
  })

  //Create Project
  $(".new-project-ajax").on('click', () => {
    $form = $('#new-project-modal').find('form');
    url = $form.attr('action');
    teamMembers = $form.find("#project-team").multipleSelect("getSelects");
    jsonForm = $form.serializeFormJSON();
    delete jsonForm["team"];
    jsonForm.team = teamMembers;
    $.ajax({
      method: 'POST',
      url: url,
      dataType: 'json',
      contentType: 'application/json',
      data: JSON.stringify(jsonForm),
      success: function( response ) {
        location.reload();
      }
    })
  })

  //Edit Project
  $(".edit-project-ajax").on('click', () => {
    $form = $('#edit-project-modal').find('form');
    url = $form.attr('action');
    teamMembers = $form.find("#project-team-edit").multipleSelect("getSelects");
    jsonForm = $form.serializeFormJSON();
    delete jsonForm["team"];
    jsonForm.team = teamMembers;
    $.ajax({
      method: 'PUT',
      url: url,
      dataType: 'json',
      contentType: 'application/json',
      data: JSON.stringify(jsonForm),
      success: function( response ) {
        location.reload();
      }
    })
  })

  $(".change-state-project-ajax").on('click', () => {
    let projectId;
    projectId = $(".change-state-project-ajax").attr('data-id');
    if (projectId === undefined){
      projectId = window.location.pathname.split('/');
      projectId = projectId[projectId.length - 1];
    }
    url = `/projects/edit/changeState/${projectId}`
    $.ajax({
      method: 'PUT',
      url: url,
      dataType: 'json',
      contentType: 'application/json',
      success: function( response ) {
        if ($(".change-state-project-ajax").attr('data-id') === undefined)
          $(location).attr("href", '/projects');
        else
          location.reload();
      }
    })
  })

  //Delete Project
  $(".delete-project-ajax").on('click', () => {
    id = $('.delete-project-ajax').attr('data-id');
    manager = $('.delete-project-ajax').attr('data-manager');
    $.ajax({
      method: 'DELETE',
      url: '/projects/delete',
      contentType: "application/x-www-form-urlencoded; charset=UTF-8",
      data: { id: id, manager: manager },
      success: function( response ) {
        location.reload();
      }
    })
  })

  // Create new story
  $(".new-story-ajax").on('click', () => {
    let projectId = window.location.pathname.split('/');
    $form = $('#new-story-modal').find('form');
    url = $form.attr('action');
    jsonForm = $form.serializeFormJSON();
    jsonForm.project_id = projectId[projectId.length - 1];
    jsonForm.state = 'CREATED';
    $.ajax({
      method: 'POST',
      url: url,
      dataType: 'json',
      contentType: 'application/json',
      data: JSON.stringify(jsonForm),
      success: function( response ) {
        location.reload();
      }
    })
  })

  $(".retrospective-ajax").on('click', () => {
    $form = $('#retrospective-modal').find('form');
    let data = $form.serializeFormJSON();
    jsonForm = {retrospective : data};
    url = $form.attr('action');
    $.ajax({
      method: 'PUT',
      url: url,
      dataType: 'json',
      contentType: 'application/json',
      data: JSON.stringify(jsonForm),
      success: function( response ) {
        $('#retrospective-modal').modal('toggle');
      }
    })
  })

  // Select one
  $("#project-owner, #project-manager, #project-owner-edit, #project-manager-edit").multipleSelect({
    placeholder: "Select a user",
    selectAll: false,
    single: true
  }).siblings().removeClass('form-control');

  // Select multiple for projects
  $("#project-team, #project-team-edit").multipleSelect({
    placeholder: "Select users",
    selectAll: false,
  }).siblings().removeClass('form-control');

  // Select multiple for users
  $("#user-abilities").multipleSelect({
    placeholder: "Select abilities",
    filter: true,
    multiple: true,
    position: 'top'
  }).siblings().removeClass('form-control');

  $('.open-edit-user-modal').on('click', () => {
    let abilities = JSON.parse($('#user-abilities').attr('data-abilities'));
    $("#user-abilities").multipleSelect("setSelects", abilities);
  })

  $('.open-edit-project-modal').on('click', () => {
    let manager = $('#project-manager-edit').attr('data-projectManager').split(' ');
    let owner = $('#project-owner-edit').attr('data-productOwner').split(' ');
    let team = JSON.parse($('#project-team-edit').attr('data-team'));
    $("#project-manager-edit").multipleSelect("setSelects", manager);
    $("#project-owner-edit").multipleSelect("setSelects", owner);
    $("#project-team-edit").multipleSelect("setSelects", team);
  })

  if($('.abilities-count').length){
    abilities = $('.abilities-count').attr('data-abilities')
    abilitiesCount = (abilities.match(/,/g) || []).length;
    abilitiesCount = abilitiesCount == 0 && abilities == ''  ? 0 : abilitiesCount + 1 ;
    abilitiesCount.toString();
    $('.abilities-count').prepend(abilitiesCount);
  }
})
