const projectId = $('#stories').attr('data-project-id');

$.fn.serializeFormJSON = function () {
  var o = {};
  var a = this.serializeArray();
  $.each(a, function () {
    if (o[this.name]) {
      if (!o[this.name].push) {
        o[this.name] = [o[this.name]];
      }
      o[this.name].push(this.value || '');
    } else {
      o[this.name] = this.value || '';
    }
  });
  return o;
};

const stories = new Vue({
  el:'#stories',
  data:{
    stories:[]
  },
  methods:{
    postApproved: function(event, storyId){
      const url = `/stories/edit/${storyId}`
      const $switch = $(event.target);
      const approved = $switch.attr('data-approved')
      let data;

      if(approved == "false" || approved == undefined){
        data = { approved: true }
      }else{
        data = { approved: false }
      }

      $.ajax({
        method: 'PUT',
        url: url,
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify(data),
        success: function( response ) {
          $switch.attr('data-approved', response.data._approved);
        }
      })
    },
    storyToModal: function(event, storyId) {
      $form = $('#show-story-modal').find('form');
      $card = $form.find('.card');

      fetch(`/stories/one/${storyId}`)
      .then(response => response.json())
      .then((json) => {
        const story = json.data;

        $form.find('#story-narrative-se').val(story._narrative);
        $form.find('#story-priority-se').val(story._priority);
        $form.find('#story-how-se').val(story._how);
        $form.find('#story-size-se').val(story._size);
        $form.find('#story-i-want-se').val(story._i_want);
        $form.find('#story-so-that-se').val(story._so_that);
        $form.find('#story-criteria-se').val(story._criteria);
        $form.find('#story-given-se').val(story._given);
        $form.find('#story-when-se').val(story._when);
        $form.find('#story-then-se').val(story._then);
        $form.find('#story-estimated-time-se').val(story._estimated_time);
        $form.find('.edit-story-btn, .delete-story-btn').attr('data-story-id', story._id);
      });
    },
    postStory: function(event) {
      const storyId = $(event.target).attr('data-story-id');
      const url = `/stories/edit/${storyId}`
      const $form = $('#show-story-modal').find('form');
      const jsonForm = $form.serializeFormJSON();

      $.ajax({
        method: 'PUT',
        url: url,
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify(jsonForm),
        success: function( response ) {
          const $modal = $('#show-story-modal');
          const $tr = $("tr[data-story-id='" + storyId + "']");

          $tr.find('.story-narrative-td a span').text(response.data._narrative);
          $tr.find('.story-size-td span').text(response.data._size);
          $tr.find('.story-priority-td span').text(response.data._priority);
          $tr.find('.story-state-td span').text(response.data._state);
          $modal.modal('toggle');
        }
      })
    },
    deleteStory: function(event) {
      const storyId = $(event.target).attr('data-story-id');
      const url = `/stories/delete/`

      $.ajax({
        method: 'DELETE',
        url: url,
        contentType: "application/x-www-form-urlencoded; charset=UTF-8",
        data: { id: storyId },
        success: function( response ) {
          const $modal = $('#show-story-modal');
          const $tr = $("tr[data-story-id='" + storyId + "']");

          $tr.remove();
          $modal.modal('toggle');
        }
      })
    }
  },
  created(){
    fetch(`/stories/${projectId}`)
    .then(response => response.json())
    .then(json => {
      this.stories = json.data;
    });
  }
});
